 # Application deployment with Ansible

Deploy Nexus, Jenkins, and a Java project with Ansible automation.

## Intro

Software deployment automation with Ansible.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Ansible v2.12.10
